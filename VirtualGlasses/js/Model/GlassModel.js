class Glassesinfo {
    constructor(id, brand, name, color, price, description) {
        this.id = id;
        this.brand = brand;
        this.name = name;
        this.color = color;
        this.price = price;
        this.description = description;
    }
}